/**
 * 
 */

package iia.jeux.alg;

import java.util.ArrayList;

import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;

public class AlphaBeta implements AlgoJeu {

    /**
     * La profondeur de recherche par défaut
     */
    private final static int PROFMAXDEFAUT = 4;
    private final static int INFINITY = 9999999;

    // -------------------------------------------
    // Attributs
    // -------------------------------------------

    /**
     * La profondeur de recherche utilisée pour l'algorithme
     */
    private int profMax = PROFMAXDEFAUT;

    /**
     * L'heuristique utilisée par l'algorithme
     */
    private Heuristique h;

    /**
     * Le joueur Min (l'adversaire)
     */
    private Joueur joueurMin;

    /**
     * Le joueur Max (celui dont l'algorithme de recherche adopte le point de vue)
     */
    private Joueur joueurMax;

    /**
     * Le nombre de noeuds développé par l'algorithme (intéressant pour se faire une
     * idée du nombre de noeuds développés)
     */
    private int nbnoeuds;

    /**
     * Le nombre de feuilles évaluées par l'algorithme
     */
    private int nbfeuilles;

    // -------------------------------------------
    // Constructeurs
    // -------------------------------------------
    public AlphaBeta(Heuristique h, Joueur joueurMax, Joueur joueurMin) {
        this(h, joueurMax, joueurMin, PROFMAXDEFAUT);
    }

    public AlphaBeta(Heuristique h, Joueur joueurMax, Joueur joueurMin, int profMaxi) {
        this.h = h;
        this.joueurMin = joueurMin;
        this.joueurMax = joueurMax;
        profMax = profMaxi;
        // System.out.println("Initialisation d'un MiniMax de profondeur " + profMax);
    }

    // -------------------------------------------
    // Méthodes de l'interface AlgoJeu
    // -------------------------------------------
    public CoupJeu meilleurCoup(PlateauJeu p) {
        ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMax);
        int alpha = -INFINITY;
        int beta = INFINITY;
        int best = alpha;
        CoupJeu mc = cp.get(0);
        for (CoupJeu c : cp) {
            PlateauJeu tmp = p.copy();
            tmp.joue(this.joueurMax, c);
            alpha = minMax(tmp, 1, alpha, beta);

            if (alpha > best) {
                best = alpha;
                mc = c;
            }
        }
        return mc;
    }

    // -------------------------------------------
    // Méthodes publiques
    // -------------------------------------------
    public String toString() {
        return "AlphaBeta(ProfMax=" + profMax + ")";
    }

    // -------------------------------------------
    // Méthodes internes
    // -------------------------------------------

    // A vous de jouer pour implanter Minimax

    private int maxMin(PlateauJeu p, int prof, int alpha, int beta) {
        if (p.finDePartie() || prof >= this.profMax) {
            this.nbfeuilles++;
            return this.h.eval(p, this.joueurMax);
        } else {
            this.nbnoeuds++;
            ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMax);
            for (CoupJeu c : cp) {
                PlateauJeu tmp = p.copy();
                tmp.joue(this.joueurMax, c);
                alpha = Math.max(alpha, minMax(tmp, prof + 1, alpha, beta));
                if (alpha >= beta) {
                    return beta;
                }
            }
            return alpha;
        }
    }

    private int minMax(PlateauJeu p, int prof, int alpha, int beta) {
        if (p.finDePartie() || prof >= this.profMax) {
            this.nbfeuilles++;
            return this.h.eval(p, this.joueurMin);
        } else {
            this.nbnoeuds++;
            ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMin);
            for (CoupJeu c : cp) {
                PlateauJeu tmp = p.copy();
                tmp.joue(this.joueurMin, c);
                beta = Math.min(beta, maxMin(tmp, prof + 1, alpha, beta));
                if (alpha >= beta) {
                    return alpha;
                }
            }
            return beta;
        }
    }

    public int getNbNoeuds() {
        return this.nbnoeuds;
    }

    public int getNbFeuilles() {
        return this.nbfeuilles;
    }

}
