/**
 * 
 */

package iia.jeux.alg;

import java.util.ArrayList;

import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;

public class Minimax implements AlgoJeu {

  /**
   * La profondeur de recherche par défaut
   */
  private final static int PROFMAXDEFAUT = 4;
  private final static int INFINITY = 9999999;

  // -------------------------------------------
  // Attributs
  // -------------------------------------------

  /**
   * La profondeur de recherche utilisée pour l'algorithme
   */
  private int profMax = PROFMAXDEFAUT;

  /**
   * L'heuristique utilisée par l'algorithme
   */
  private Heuristique h;

  /**
   * Le joueur Min (l'adversaire)
   */
  private Joueur joueurMin;

  /**
   * Le joueur Max (celui dont l'algorithme de recherche adopte le point de vue)
   */
  private Joueur joueurMax;

  /**
   * Le nombre de noeuds développé par l'algorithme (intéressant pour se faire une
   * idée du nombre de noeuds développés)
   */
  private int nbnoeuds;

  /**
   * Le nombre de feuilles évaluées par l'algorithme
   */
  private int nbfeuilles;

  // -------------------------------------------
  // Constructeurs
  // -------------------------------------------
  public Minimax(Heuristique h, Joueur joueurMax, Joueur joueurMin) {
    this(h, joueurMax, joueurMin, PROFMAXDEFAUT);
  }

  public Minimax(Heuristique h, Joueur joueurMax, Joueur joueurMin, int profMaxi) {
    this.h = h;
    this.joueurMin = joueurMin;
    this.joueurMax = joueurMax;
    profMax = profMaxi;
    // System.out.println("Initialisation d'un MiniMax de profondeur " + profMax);
  }

  // -------------------------------------------
  // Méthodes de l'interface AlgoJeu
  // -------------------------------------------
  public CoupJeu meilleurCoup(PlateauJeu p) {
    /* A vous de compléter le corps de ce fichier */

    ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMax);

    CoupJeu c = null;
    int max = -INFINITY;

    for (CoupJeu c_bis : cp) {
      PlateauJeu tmp = p.copy();
      tmp.joue(this.joueurMax, c_bis);
      int x = minMax(tmp, 1);
      if (x > max) {
        c = c_bis;
        max = x;
      }
    }
    return c;

  }

  // -------------------------------------------
  // Méthodes publiques
  // -------------------------------------------
  public String toString() {
    return "MiniMax(ProfMax=" + profMax + ")";
  }

  // -------------------------------------------
  // Méthodes internes
  // -------------------------------------------

  // A vous de jouer pour implanter Minimax

  private int maxMin(PlateauJeu p, int prof) {
    if (p.finDePartie() || prof >= this.profMax) {
      this.nbfeuilles++;
      return this.h.eval(p, this.joueurMax);
    } else {
      this.nbnoeuds++;
      int max = -INFINITY;
      ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMax);
      for (CoupJeu c : cp) {
        PlateauJeu tmp = p.copy();
        tmp.joue(this.joueurMax, c);
        max = Math.max(max, minMax(tmp, prof + 1));
      }
      return max;
    }
  }

  private int minMax(PlateauJeu p, int prof) {
    if (p.finDePartie() || prof >= this.profMax) {
      this.nbfeuilles++;
      return this.h.eval(p, this.joueurMin);
    } else {
      this.nbnoeuds++;
      int min = INFINITY;
      ArrayList<CoupJeu> cp = p.coupsPossibles(this.joueurMin);
      for (CoupJeu c : cp) {
        PlateauJeu tmp = p.copy();
        tmp.joue(this.joueurMin, c);
        min = Math.min(min, maxMin(tmp, prof + 1));
      }
      return min;
    }
  }

  public int getNbNoeuds() {
    return this.nbnoeuds;
  }

  public int getNbFeuilles() {
    return this.nbfeuilles;
  }

}
