package jeux.dominos;

import iia.jeux.alg.Heuristique;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;

public class HeuristiquesDominos {

	private static final int INFINITY = 99999999;

	public static Heuristique hblanc = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			PlateauDominos pd = (PlateauDominos) p;
			int nb_b = pd.nbCoupsBlanc();
			int nb_n = pd.nbCoupsNoir();

			if (pd.isJoueurBlanc(j) && nb_b == 0) {
				return -INFINITY;
			}

			if (pd.isJoueurNoir(j) && nb_n == 0) {
				return INFINITY;
			}

			return nb_b - nb_n;
		}
	};

	public static Heuristique hnoir = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			PlateauDominos pd = (PlateauDominos) p;
			int nb_b = pd.nbCoupsBlanc();
			int nb_n = pd.nbCoupsNoir();

			if (pd.isJoueurBlanc(j) && nb_b == 0) {
				return INFINITY;
			}

			if (pd.isJoueurNoir(j) && nb_n == 0) {
				return -INFINITY;
			}

			return nb_b - nb_n;
		}
	};

}
